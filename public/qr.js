var canvas = document.getElementById("qr-display");
var ctx = canvas.getContext("2d");

var symbolVersion = 4;
var symbolSize = 17 + 4 * symbolVersion;
var scaleUpFactor = 10;
var displaySize = symbolSize * scaleUpFactor;
var symbolState = new Array(symbolSize);
var busyModules = new Array(symbolSize);
var dataAndECCBits = [];

var maskingPattern = 0;          // 0 <=    maskingPattern      <= 7
var errorCorrectionLevel = 3;    // 0 <=  errorCorrectionLevel  <= 3
var drawBitsOrder = true;
var qrContent = "https://michaelpaper.xyz/"
var drawGrid = true;

// Arrays to keep track of the pixel <-> content mappings
var charPositionInBits = new Array(qrContent.length);
var bitPositionInString = new Array(symbolSize * symbolSize);
var charPositionInBitBlocks = new Array();
var blockBitPositionInString = new Array();
var charPositionInFinalBits = new Array(qrContent.length);
var bitFinalPositionInString = new Array(symbolSize * symbolSize);
var charCoordinatesOnSymbol = new Array(qrContent.length);
var pixelPositionInString = new Array(symbolSize);
var pixelPositionInPositionPattern = new Array(symbolSize);
var pixelPositionInTimingPatterns = new Array(symbolSize);
var pixelPositionInAlignmentPatterns = new Array(symbolSize);
var pixelPositionInMetadata = new Array(symbolSize);
var positionPatternCoordinatesOnSymbol = {"tr": [], "tl": [], "bl": []};
var timingPatternsCoordinatesOnSymbol = new Array((symbolSize - 16) * 2);
var alignmentPatternsCoordinatesOnSymbol = [];
var metadataCoordinatesOnSymbol = [];
var previousMouseCoord = {x:0,y:0};

canvas.style.width = displaySize.toString();
canvas.style.height = displaySize.toString();

function drawFrames() {
  ctx.beginPath();
  ctx.strokeStyle = "rgb(0,0,255)";
  for (let i = 1; i < symbolSize; i++) {
    ctx.moveTo(scaleUpFactor * i, 0);
    ctx.lineTo(scaleUpFactor * i, scaleUpFactor * symbolSize);
    ctx.moveTo(0, scaleUpFactor * i);
    //ctx.lineTo(displaySize - 1, scaleUpFactor * i);
    ctx.lineTo(scaleUpFactor * symbolSize, scaleUpFactor * i);
  }
  ctx.stroke();
}

function setModule(x, y, value, formatting) {
  if (value) {
    ctx.fillStyle = "rgb(0,0,0)";
  } else {
    ctx.fillStyle = "rgb(255,255,255)";
  }

  symbolState[x][y] = value;
  if (formatting) {
    busyModules[x][y] = true;
    //ctx.fillStyle = "rgb(0,0,0)";
  }
  ctx.fillRect(scaleUpFactor * x, scaleUpFactor * y, scaleUpFactor, scaleUpFactor);
  //ctx.beginPath();
  //ctx.arc(scaleUpFactor * x + scaleUpFactor/2, scaleUpFactor * y + scaleUpFactor/2, scaleUpFactor/2, 0, 2*Math.PI, true)
  //ctx.fill();
}


function drawPositionDetectionPatterns() {
  for (let i = 0; i < 8; i++) {
    for (let j = 0; j < 8; j++) {
      // top-left block
      setModule(i, j, false, true);
      positionPatternCoordinatesOnSymbol["tl"].push({x:i,y:j});
      pixelPositionInPositionPattern[i][j] = ["tl"];
      // bottom-left block
      setModule(i, symbolSize - 8 + j, false, true);
      positionPatternCoordinatesOnSymbol["bl"].push({x:i,y:symbolSize - 8 + j});
      pixelPositionInPositionPattern[i][symbolSize - 8 + j] = ["bl"];
      // top-right block
      setModule(symbolSize - 8 + i, j, false, true);
      positionPatternCoordinatesOnSymbol["tr"].push({x:symbolSize - 8 + i,y:j});
      pixelPositionInPositionPattern[symbolSize - 8 + i][j] = ["tr"];
    }
  }

  for (let i = 0; i < 7; i++) {
    // top-left block
    setModule(0, i, true, true);
    setModule(i, 0, true, true);
    setModule(6, i, true, true);
    setModule(i, 6, true, true);
    // bottom-left block
    setModule(0, symbolSize - 7 + i, true, true);
    setModule(i, symbolSize - 7, true, true);
    setModule(6, symbolSize - 7 + i, true, true);
    setModule(i, symbolSize - 1, true, true);
    // top-right block
    setModule(symbolSize - 7 + i, 0, true, true);
    setModule(symbolSize - 7, i, true, true);
    setModule(symbolSize - 7 + i, 6, true, true);
    setModule(symbolSize - 1, i, true, true);
  }


  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      // top-left block
      setModule(i + 2, j + 2, true, true);
      // bottom-left block
      setModule(i + 2, symbolSize - 5 + j, true, true);
      // top-left block
      setModule(symbolSize - 5 + i, j + 2, true, true);
    }
  }
}

var tableE1 = {
  1: [],
  2: [6, 18],
  3: [6, 22],
  4: [6, 26],
  5: [6, 30],
  6: [6, 34],
  7: [6, 22, 38],
  8: [6, 24, 42],
  9: [6, 26, 46],
  10: [6, 28, 50],
  11: [6, 30, 54],
  12: [6, 32, 58],
  13: [6, 34, 62],
  14: [6, 26, 46, 66],
  15: [6, 26, 48, 70],
  16: [6, 26, 50, 74],
  17: [6, 30, 54, 78],
  18: [6, 30, 56, 82],
  19: [6, 30, 58, 86],
  20: [6, 34, 62, 90],
  21: [6, 28, 50, 72, 94],
  22: [6, 26, 50, 74, 98],
  23: [6, 30, 54, 78, 102],
  24: [6, 28, 54, 80, 106],
  25: [6, 32, 58, 84, 110],
  26: [6, 30, 58, 86, 114],
  27: [6, 34, 62, 90, 118],
  28: [6, 26, 50, 74, 98, 122],
  29: [6, 30, 54, 78, 102, 126],
  30: [6, 26, 52, 78, 104, 130],
  31: [6, 30, 56, 82, 108, 134],
  32: [6, 34, 60, 86, 112, 138],
  33: [6, 30, 58, 86, 114, 142],
  34: [6, 34, 62, 90, 118, 146],
  35: [6, 30, 54, 78, 102, 126, 150],
  36: [6, 24, 50, 76, 102, 128, 154],
  37: [6, 28, 54, 80, 106, 132, 158],
  38: [6, 32, 58, 84, 110, 136, 162],
  39: [6, 26, 54, 82, 110, 138, 166],
  40: [6, 30, 58, 86, 114, 142, 170],
}

var tableD1 = {
  7: [false, false, false, true, true, true, true, true, false, false, true, false, false, true, false, true, false, false],
  8: [false, false, true, false, false, false, false, true, false, true, true, false, true, true, true, true, false, false],
  9: [false, false, true, false, false, true, true, false, true, false, true, false, false, true, true, false, false, true],
  10: [false, false, true, false, true, false, false, true, false, false, true, true, false, true, false, false, true, true],
  11: [false, false, true, false, true, true, true, false, true, true, true, true, true, true, false, true, true, false],
  12: [false, false, true, true, false, false, false, true, true, true, false, true, true, false, false, false, true, false],
  13: [false, false, true, true, false, true, true, false, false, false, false, true, false, false, false, true, true, true],
  14: [false, false, true, true, true, false, false, true, true, false, false, false, false, false, true, true, false, true],
  15: [false, false, true, true, true, true, true, false, false, true, false, false, true, false, true, false, false, false],
  16: [false, true, false, false, false, false, true, false, true, true, false, true, true, true, true, false, false, false],
  17: [false, true, false, false, false, true, false, true, false, false, false, true, false, true, true, true, false, true],
  18: [false, true, false, false, true, false, true, false, true, false, false, false, false, true, false, true, true, true],
  19: [false, true, false, false, true, true, false, true, false, true, false, false, true, true, false, false, true, false],
  20: [false, true, false, true, false, false, true, false, false, true, true, false, true, false, false, true, true, false],
  21: [false, true, false, true, false, true, false, true, true, false, true, false, false, false, false, false, true, true],
  22: [false, true, false, true, true, false, true, false, false, false, true, true, false, false, true, false, false, true],
  23: [false, true, false, true, true, true, false, true, true, true, true, true, true, false, true, true, false, false],
  24: [false, true, true, false, false, false, true, true, true, false, true, true, false, false, false, true, false, false],
  25: [false, true, true, false, false, true, false, false, false, true, true, true, true, false, false, false, false, true],
  26: [false, true, true, false, true, false, true, true, true, true, true, false, true, false, true, false, true, true],
  27: [false, true, true, false, true, true, false, false, false, false, true, false, false, false, true, true, true, false],
  28: [false, true, true, true, false, false, true, true, false, false, false, false, false, true, true, false, true, false],
  29: [false, true, true, true, false, true, false, false, true, true, false, false, true, true, true, true, true, true],
  30: [false, true, true, true, true, false, true, true, false, true, false, true, true, true, false, true, false, true],
  31: [false, true, true, true, true, true, false, false, true, false, false, true, false, true, false, false, false, false],
  32: [true, false, false, false, false, false, true, false, false, true, true, true, false, true, false, true, false, true],
  33: [true, false, false, false, false, true, false, true, true, false, true, true, true, true, false, false, false, false],
  34: [true, false, false, false, true, false, true, false, false, false, true, false, true, true, true, false, true, false],
  35: [true, false, false, false, true, true, false, true, true, true, true, false, false, true, true, true, true, true],
  36: [true, false, false, true, false, false, true, false, true, true, false, false, false, false, true, false, true, true],
  37: [true, false, false, true, false, true, false, true, false, false, false, false, true, false, true, true, true, false],
  38: [true, false, false, true, true, false, true, false, true, false, false, true, true, false, false, true, false, false],
  39: [true, false, false, true, true, true, false, true, false, true, false, true, false, false, false, false, false, true],
  40: [true, false, true, false, false, false, true, true, false, false, false, true, true, false, true, false, false, true],
}


function drawAlignmentPatternCentered(x, y) {
  // pixel in the center
  setModule(x, y, true, true);
  alignmentPatternsCoordinatesOnSymbol.push({x:x,y:y});
  pixelPositionInAlignmentPatterns[x][y] = true;
  // white pixels around
  for (let i = x-1; i < x + 2; i++) {
    for (let j = y-1; j < y + 2; j++) {
      if (j != y || i != x) {
	setModule(i, j, false, true);
        alignmentPatternsCoordinatesOnSymbol.push({x:i,y:j});
        pixelPositionInAlignmentPatterns[i][j] = true;
      }
    }
  }
  // black pixels around
  for (let i = x-2; i < x + 3; i++) {
    for (let j = y-2; j < y + 3; j++) {
      if (i == x-2 || j == y-2 || i == x+2 || j == y+2) {
	setModule(i, j, true, true);
        alignmentPatternsCoordinatesOnSymbol.push({x:i,y:j});
        pixelPositionInAlignmentPatterns[i][j] = true;
      }
    }
  }
}

function drawAlignmentPatterns() {
  rowColumnCoordinates = tableE1[symbolVersion];
  for (let i = 0; i < rowColumnCoordinates.length; i++) {
    for (let j = 0; j < rowColumnCoordinates.length; j++) {
      // don't plug the alignment pattern if it's in a position detection
      // pattern
      if (i == 0 && j == 0) {
	continue;
      } else if (i == 0 && j == rowColumnCoordinates.length - 1) {
	continue;
      } else if (i == rowColumnCoordinates.length - 1 && j == 0) {
	continue;
      } else {
	drawAlignmentPatternCentered(rowColumnCoordinates[i],rowColumnCoordinates[j]);
      }
    }
  }
}

function drawTimingPatterns() {
  for (let i = 0; i < symbolSize - 16; i++) {
    setModule(i+8, 6, i % 2 == 0, true);
    timingPatternsCoordinatesOnSymbol[2 * i] = {x:i+8,y:6};
    pixelPositionInTimingPatterns[i+8][6] = true;
    setModule(6, i+8, i % 2 == 0, true);
    timingPatternsCoordinatesOnSymbol[2 * i + 1] = {x:6,y:i+8};
    pixelPositionInTimingPatterns[6][i+8] = true;
  }
}

function printPolynomial(p) {
  pString = "";
  isPZero = true;
  for (let i = 0; i < p.length; i++) {
    if (p[i]) {
      if (!isPZero) {
	pString += " + ";
      }
      if (i == 0) {
	pString += "1";
      } else {
	pString += "x^" + i;
      }
      isPZero = false;
    }
  }
  if (isPZero) {
    pString = "0";
  }

  console.log(pString);
}

function isPolynomialZero(p) {
  let isPZero = true;
  for (let i = 0; i < p.length && isPZero; i++) {
    isPZero = !p[i];
  }
  return isPZero;
}

function polynomialDegree(p) {
  degree = 0;
  for (let i = 0; i < p.length; i++) {
    if (p[i]) {
      degree = i;
    }
  }

  return degree;
}

function polynomialSubstract(p, q) {
  let degreeP = polynomialDegree(p);
  let degreeQ = polynomialDegree(q);
  let degreeR = Math.max(degreeP, degreeQ);
  let r = new Array(degreeR);
  for (let i = 0; i < Math.min(degreeP, degreeQ); i++) {
    if (p[i] ^ q[i]) {
      r[i] = true;
    } else {
      r[i] = false;
    }
  }
  for (let i = Math.min(degreeP, degreeQ); i < Math.max(degreeP, degreeQ); i++) {
    if (degreeP > degreeQ) {
      r[i] = p[i];
    } else {
      r[i] = q[i];
    }
  }

  return r;
}

// Multiplies a polynomial p by the polynomial "x^t"
function polynomialMultiplyByTerm(p, t) {
  let degreeP = polynomialDegree(p);
  let result = new Array(degreeP + t);
  for (let i = 0; i < t; i++) {
    result[i] = false;
  }
  for (let i = 0; i < degreeP + 1; i++) {
    result[i + t] = p[i];
  }


  return result;
}

function polynomialDivisionRemainder(n, d) {
  if (isPolynomialZero(d)) {
    console.log("WARGNING: dividing by NULL polynomial");
    return [];
  }

  let r = Array.from(n);

  while (!isPolynomialZero(r) && polynomialDegree(r) >= polynomialDegree(d)) {
    degreeT = polynomialDegree(r) - polynomialDegree(d);
    t = new Array(degreeT); 
    for (let i = 0; i < degreeT - 1; i++) {
      t[i] = false;
    }
    t[degreeT - 1] = true;
    r = polynomialSubstract(r, polynomialMultiplyByTerm(d, degreeT));
  }
  
  return r;
}

function drawFormatInformation() {
  let formatInformationValue = 8 * (errorCorrectionLevel ^ 1) + maskingPattern;
  let formatInformationString = new Array(5);
  for (let i = 0; i < 5; i++) {
    formatInformationString[i] = formatInformationValue % 2 == 1;
    formatInformationValue = Math.floor(formatInformationValue / 2);
  }
  let raisedPolynomial = new Array(12);
  for (let i = 0; i < 10; i++) {
    raisedPolynomial[i] = false;
  }
  for (let i = 0; i < 5; i++) {
    raisedPolynomial[i + 10] = formatInformationString[i];
  }

  g = [true, true, true, false, true, true, false, false, true, false, true];
  remainder = polynomialDivisionRemainder(raisedPolynomial, g);
  infoWithErrorCorrection = new Array(15);
  for (let i = 0; i < 10; i++) {
    if (i < remainder.length) {
      infoWithErrorCorrection[i] = remainder[i];
    } else {
      infoWithErrorCorrection[i] = false;
    }
  }
  for (let i = 0; i < 5; i++) {
    infoWithErrorCorrection[i + 10] = formatInformationString[i];
  }
  mask = [false, true, false, false, true, false, false, false, false, false,
          true, false, true, false, true];
  result = new Array(15);
  for (let i = 0; i < 15; i++) {
    if (infoWithErrorCorrection[i] ^ mask[i]) {
      result[i] = true;
    } else {
      result[i] = false;
    }
  }

  // Displaying the code and the error correction on the symbol
  for (let i = 0; i < 6; i++) {
    setModule(8, i, result[i], true);
    metadataCoordinatesOnSymbol.push({x:8,y:i});
    pixelPositionInMetadata[8][i] = true;
  }
  setModule(8, 7, result[6], true);
  metadataCoordinatesOnSymbol.push({x:8,y:7});
  pixelPositionInMetadata[8][7] = true;
  setModule(8, 8, result[7], true);
  metadataCoordinatesOnSymbol.push({x:8,y:8});
  pixelPositionInMetadata[8][8] = true;
  setModule(7, 8, result[8], true);
  metadataCoordinatesOnSymbol.push({x:7,y:8});
  pixelPositionInMetadata[7][8] = true;
  for (let i = 0; i < 6; i++) {
    setModule(i, 8, result[14 - i], true);
    metadataCoordinatesOnSymbol.push({x:i,y:8});
    pixelPositionInMetadata[i][8] = true;
  }
  for (let i = 0; i < 8; i++) {
    setModule(symbolSize - i - 1, 8, result[i], true);
    metadataCoordinatesOnSymbol.push({x:symbolSize-i-1,y:8});
    pixelPositionInMetadata[symbolSize-i-1][8] = true;
  }
  for (let i = 0; i < 7; i++) {
    setModule(8, symbolSize - i - 1, result[14 - i], true);
    metadataCoordinatesOnSymbol.push({x:8,y:symbolSize-i-1});
    pixelPositionInMetadata[8][symbolSize-i-1] = true;
  }
  setModule(8, symbolSize - 8, true, true);
  metadataCoordinatesOnSymbol.push({x:8,y:symbolSize-8});
  pixelPositionInMetadata[8][symbolSize-8] = true;
}

function drawVersionInformation() {
  if (symbolVersion < 7) {
    return;
  }
  //versionInformationBS = tableD1[symbolVersion].reverse();
  versionInformationBS = new Array(tableD1[symbolVersion].length);
  for (let i = 0; i < versionInformationBS.length; i++) {
    versionInformationBS[i] = tableD1[symbolVersion][i];
  }
  versionInformationBS.reverse();
  for (let i = 0; i < 18; i++) {
    setModule(symbolSize - 11 + (i % 3), Math.floor(i / 3), versionInformationBS[i], true);
    metadataCoordinatesOnSymbol.push({x:symbolSize - 11 + (i % 3),y:Math.floor(i / 3)});
    pixelPositionInMetadata[symbolSize - 11 + (i % 3)][Math.floor(i / 3)] = true;
  }
  for (let i = 0; i < 18; i++) {
    setModule(Math.floor(i / 3), symbolSize - 11 + (i % 3), versionInformationBS[i], true);
    metadataCoordinatesOnSymbol.push({x:Math.floor(i / 3),y:symbolSize - 11 + (i % 3)});
    pixelPositionInMetadata[Math.floor(i / 3)][symbolSize - 11 + (i % 3)] = true;
  }
}

function intToBytes(n, size) {
  let result = [];
  let i = 0;
  while (n > 0 && i < size) {
    if (n % 2) {
      result[i] = true;
    } else {
      result[i] = false;
    }
    i += 1;
    n = Math.floor(n/2);
  }
  while (i < size) {
    result[i] = false;
    i += 1;
  }

  return result.reverse();
}

var tableA1 = {
  2: [25, 1],
  5: [113, 164, 166, 119, 10],
  6: [166, 0, 134, 5, 176, 15],
  7: [87, 229, 146, 149, 238, 102, 21],
  8: [175, 238, 208, 249, 215, 252, 196, 28],
  10: [251, 67, 46, 61, 118, 70, 64, 94, 32, 45],
  13: [74, 152, 176, 100, 86, 100, 106, 104, 130, 218, 206, 140, 78],
  14: [199, 249, 155, 48, 190, 124, 218, 137, 216, 87, 207, 59, 22, 91],
  15: [8, 183, 61, 91, 202, 37, 51, 58, 58, 237, 140, 124, 5, 99, 105],
  16: [120, 104, 107, 109, 102, 161, 76, 3, 91, 191, 147, 169, 182, 194, 225, 120],
  17: [43, 139, 206, 78, 43, 239, 123, 206, 214, 147, 24, 99, 150, 39, 243, 163, 136],
  18: [215, 234, 158, 94, 184, 97, 118, 170, 79, 187, 152, 148, 252, 179, 5, 98, 96, 153],
  20: [17, 60, 79, 50, 61, 163, 26, 187, 202, 180, 221, 225, 83, 239, 156, 164, 212, 212, 188, 190],
  22: [210, 171, 247, 242, 93, 230, 14, 109, 221, 53, 200, 74, 8, 172, 98, 80, 219, 134, 160, 105, 165, 231],
  24: [229, 121, 135, 48, 211, 117, 251, 126, 159, 180, 169, 152, 192, 226, 228, 218, 111, 0, 117, 232, 87, 96, 227, 21],  // noqa: E501
  26: [173, 125, 158, 2, 103, 182, 118, 17, 145, 201, 111, 28, 165, 53, 161, 21, 245, 142, 13, 102, 48, 227, 153, 145, 218, 70],  // noqa: E501
  28: [168, 223, 200, 104, 224, 234, 108, 180, 110, 190, 195, 147, 205, 27, 232, 201, 21, 43, 245, 87, 42, 195, 212, 119, 242, 37, 9, 123],  // noqa: E501
  30: [41, 173, 145, 152, 216, 31, 179, 182, 50, 48, 110, 86, 239, 96, 222, 125, 42, 173, 226, 193, 224, 130, 156, 37, 251, 216, 238, 40, 192, 180]  // noqa: E501
}

var galoisLog = [
  0, 0, 1, 25, 2, 50, 26, 198, 3, 223, 51, 238, 27, 104, 199, 75, 4, 100,
  224, 14, 52, 141, 239, 129, 28, 193, 105, 248, 200, 8, 76, 113, 5, 138,
  101, 47, 225, 36, 15, 33, 53, 147, 142, 218, 240, 18, 130, 69, 29, 181,
  194, 125, 106, 39, 249, 185, 201, 154, 9, 120, 77, 228, 114, 166, 6, 191,
  139, 98, 102, 221, 48, 253, 226, 152, 37, 179, 16, 145, 34, 136, 54, 208,
  148, 206, 143, 150, 219, 189, 241, 210, 19, 92, 131, 56, 70, 64, 30, 66,
  182, 163, 195, 72, 126, 110, 107, 58, 40, 84, 250, 133, 186, 61, 202, 94,
  155, 159, 10, 21, 121, 43, 78, 212, 229, 172, 115, 243, 167, 87, 7, 112,
  192, 247, 140, 128, 99, 13, 103, 74, 222, 237, 49, 197, 254, 24, 227, 165,
  153, 119, 38, 184, 180, 124, 17, 68, 146, 217, 35, 32, 137, 46, 55, 63,
  209, 91, 149, 188, 207, 205, 144, 135, 151, 178, 220, 252, 190, 97, 242,
  86, 211, 171, 20, 42, 93, 158, 132, 60, 57, 83, 71, 109, 65, 162, 31, 45,
  67, 216, 183, 123, 164, 118, 196, 23, 73, 236, 127, 12, 111, 246, 108,
  161, 59, 82, 41, 157, 85, 170, 251, 96, 134, 177, 187, 204, 62, 90, 203,
  89, 95, 176, 156, 169, 160, 81, 11, 245, 22, 235, 122, 117, 44, 215, 79,
  174, 213, 233, 230, 231, 173, 232, 116, 214, 244, 234, 168, 80, 88, 175
]

var galoisExp = [
  1, 2, 4, 8, 16, 32, 64, 128, 29, 58, 116, 232, 205, 135, 19, 38, 76, 152,
  45, 90, 180, 117, 234, 201, 143, 3, 6, 12, 24, 48, 96, 192, 157, 39, 78,
  156, 37, 74, 148, 53, 106, 212, 181, 119, 238, 193, 159, 35, 70, 140, 5,
  10, 20, 40, 80, 160, 93, 186, 105, 210, 185, 111, 222, 161, 95, 190, 97,
  194, 153, 47, 94, 188, 101, 202, 137, 15, 30, 60, 120, 240, 253, 231, 211,
  187, 107, 214, 177, 127, 254, 225, 223, 163, 91, 182, 113, 226, 217, 175,
  67, 134, 17, 34, 68, 136, 13, 26, 52, 104, 208, 189, 103, 206, 129, 31,
  62, 124, 248, 237, 199, 147, 59, 118, 236, 197, 151, 51, 102, 204, 133, 23,
  46, 92, 184, 109, 218, 169, 79, 158, 33, 66, 132, 21, 42, 84, 168, 77, 154,
  41, 82, 164, 85, 170, 73, 146, 57, 114, 228, 213, 183, 115, 230, 209, 191,
  99, 198, 145, 63, 126, 252, 229, 215, 179, 123, 246, 241, 255, 227, 219,
  171, 75, 150, 49, 98, 196, 149, 55, 110, 220, 165, 87, 174, 65, 130, 25,
  50, 100, 200, 141, 7, 14, 28, 56, 112, 224, 221, 167, 83, 166, 81, 162, 89,
  178, 121, 242, 249, 239, 195, 155, 43, 86, 172, 69, 138, 9, 18, 36, 72,
  144, 61, 122, 244, 245, 247, 243, 251, 235, 203, 139, 11, 22, 44, 88, 176,
  125, 250, 233, 207, 131, 27, 54, 108, 216, 173, 71, 142,
  1, 2, 4, 8, 16, 32, 64, 128, 29, 58, 116, 232, 205, 135, 19, 38, 76, 152,
  45, 90, 180, 117, 234, 201, 143, 3, 6, 12, 24, 48, 96, 192, 157, 39, 78,
  156, 37, 74, 148, 53, 106, 212, 181, 119, 238, 193, 159, 35, 70, 140, 5,
  10, 20, 40, 80, 160, 93, 186, 105, 210, 185, 111, 222, 161, 95, 190, 97,
  194, 153, 47, 94, 188, 101, 202, 137, 15, 30, 60, 120, 240, 253, 231, 211,
  187, 107, 214, 177, 127, 254, 225, 223, 163, 91, 182, 113, 226, 217, 175,
  67, 134, 17, 34, 68, 136, 13, 26, 52, 104, 208, 189, 103, 206, 129, 31,
  62, 124, 248, 237, 199, 147, 59, 118, 236, 197, 151, 51, 102, 204, 133, 23,
  46, 92, 184, 109, 218, 169, 79, 158, 33, 66, 132, 21, 42, 84, 168, 77, 154,
  41, 82, 164, 85, 170, 73, 146, 57, 114, 228, 213, 183, 115, 230, 209, 191,
  99, 198, 145, 63, 126, 252, 229, 215, 179, 123, 246, 241, 255, 227, 219,
  171, 75, 150, 49, 98, 196, 149, 55, 110, 220, 165, 87, 174, 65, 130, 25,
  50, 100, 200, 141, 7, 14, 28, 56, 112, 224, 221, 167, 83, 166, 81, 162, 89,
  178, 121, 242, 249, 239, 195, 155, 43, 86, 172, 69, 138, 9, 18, 36, 72,
  144, 61, 122, 244, 245, 247, 243, 251, 235, 203, 139, 11, 22, 44, 88, 176,
  125, 250, 233, 207, 131, 27, 54, 108, 216, 173, 71, 142
]


var table7 = [
  [],
  [19, 16, 13, 9],
  [34, 28, 22, 16],
  [55, 44, 34, 26],
  [80, 64, 48, 36],
  [108, 86, 62, 46],
  [136, 108, 76, 60],
  [156, 124, 88, 66],
  [194, 154, 110, 86],
  [232, 182, 132, 100],
  [274, 216, 154, 122],
  [324, 254, 180, 140],
  [370, 290, 206, 158],
  [428, 334, 244, 180],
  [461, 365, 261, 197],
  [523, 415, 295, 223],
  [589, 453, 325, 253],
  [647, 507, 367, 283],
  [721, 563, 397, 313],
  [795, 627, 445, 341],
  [861, 669, 485, 385],
  [932, 714, 512, 406],
  [1006, 782, 568, 442],
  [1094, 860, 614, 464],
  [1174, 914, 664, 514],
  [1276, 1000, 718, 538],
  [1370, 1062, 754, 596],
  [1468, 1128, 808, 628],
  [1531, 1193, 871, 661],
  [1631, 1267, 911, 701],
  [1735, 1373, 985, 745],
  [1843, 1455, 1033, 793],
  [1955, 1541, 1115, 845],
  [2071, 1631, 1171, 901],
  [2191, 1725, 1231, 961],
  [2306, 1812, 1286, 986],
  [2434, 1914, 1354, 1054],
  [2566, 1992, 1426, 1096],
  [2702, 2102, 1502, 1142],
  [2812, 2216, 1582, 1222],
  [2956, 2334, 1666, 1276],
];

var table13 = [
  [],
  [                                       // Version 1
    [{c: 26, k: 19, r: 2, reps: 1}],
    [{c: 26, k: 16, r: 4, reps: 1}],
    [{c: 26, k: 13, r: 6, reps: 1}],
    [{c: 26, k: 9, r: 8, reps: 1}],
  ],
  [                                       // Version 2
    [{c: 44, k: 34, r: 4, reps: 1}],
    [{c: 44, k: 28, r: 8, reps: 1}],
    [{c: 44, k: 22, r: 11, reps: 1}],
    [{c: 44, k: 16, r: 14, reps: 1}],
  ],
  [                                       // Version 3
    [{c: 70, k: 55, r: 7, reps: 1}],
    [{c: 70, k: 44, r: 13, reps: 1}],
    [{c: 35, k: 17, r: 9, reps: 2}],
    [{c: 35, k: 13, r: 11, reps: 2}],
  ],
  [                                       // Version 4
    [{c: 100, k: 80, r: 10, reps: 1}],
    [{c: 50, k: 32, r: 9, reps: 2}],
    [{c: 50, k: 24, r: 13, reps: 2}],
    [{c: 25, k: 9, r: 8, reps: 4}],
  ],
  [                                       // Version 5
    [{c: 134, k: 108, r: 13, reps: 1}],
    [{c: 67, k: 43, r: 12, reps: 2}],
    [{c: 33, k: 15, r: 9, reps: 2}, {c: 34, k: 16, r: 9, reps: 2}],
    [{c: 33, k: 11, r: 11, reps: 2}, {c: 34, k: 12, r: 11, reps: 2}],
  ],
  [                                       // Version 6
    [{c: 86, k: 68, r: 9, reps: 2}],
    [{c: 43, k: 27, r: 8, reps: 4}],
    [{c: 43, k: 19, r: 12, reps: 4}],
    [{c: 43, k: 15, r: 14, reps: 4}],
  ],
  [                                       // Version 7
    [{c: 98, k: 78, r: 10, reps: 2}],
    [{c: 49, k: 31, r: 9, reps: 4}],
    [{c: 32, k: 14, r: 9, reps: 2}, {c: 33, k: 15, r: 9, reps: 4}],
    [{c: 39, k: 13, r: 13, reps: 4}, {c: 40, k: 14, r: 13, reps: 1}],
  ],
  [                                       // Version 8
    [{c: 121, k: 97, r: 12, reps: 2}],
    [{c: 60, k: 38, r: 11, reps: 2}, {c: 61, k: 39, r: 11, reps: 2}],
    [{c: 40, k: 18, r: 11, reps: 4}, {c: 41, k: 19, r: 11, reps: 2}],
    [{c: 40, k: 14, r: 13, reps: 4}, {c: 41, k: 15, r: 13, reps: 2}],
  ],
  [                                       // Version 9
    [{c: 146, k: 116, r: 15, reps: 2}],
    [{c: 58, k: 36, r: 11, reps: 3}, {c: 59, k: 37, r: 11, reps: 2}],
    [{c: 36, k: 16, r: 10, reps: 4}, {c: 37, k: 17, r: 10, reps: 4}],
    [{c: 36, k: 12, r: 12, reps: 4}, {c: 37, k: 13, r: 12, reps: 4}],
  ],
  [                                       // Version 10
    [{c: 86, k: 68, r: 9, reps: 2}, {c: 87, k: 69, r: 9, reps: 2}],
    [{c: 69, k: 43, r: 13, reps: 4}, {c: 70, k: 44, r: 13, reps: 1}],
    [{c: 43, k: 19, r: 12, reps: 6}, {c: 44, k: 20, r: 12, reps: 2}],
    [{c: 43, k: 15, r: 14, reps: 6}, {c: 44, k: 16, r: 14, reps: 2}],
  ],
  [                                       // Version 11
    [{c: 101, k: 81, r: 10, reps: 4}],
    [{c: 80, k: 50, r: 15, reps: 1}, {c: 81, k: 51, r: 15, reps: 4}],
    [{c: 50, k: 22, r: 14, reps: 4}, {c: 51, k: 23, r: 14, reps: 4}],
    [{c: 36, k: 12, r: 12, reps: 3}, {c: 37, k: 13, r: 12, reps: 8}],
  ],
  [                                       // Version 12
    [{c: 116, k: 92, r: 12, reps: 2}, {c: 117, k: 93, r: 12, reps: 2}],
    [{c: 58, k: 36, r: 11, reps: 6}, {c: 59, k: 37, r: 11, reps: 2}],
    [{c: 46, k: 20, r: 13, reps: 4}, {c: 47, k: 21, r: 13, reps: 6}],
    [{c: 42, k: 14, r: 14, reps: 7}, {c: 43, k: 15, r: 14, reps: 4}],
  ],
  [                                       // Version 13
    [{c: 133, k: 107, r: 13, reps: 4}],
    [{c: 59, k: 37, r: 11, reps: 8}, {c: 60, k: 38, r: 11, reps: 1}],
    [{c: 44, k: 20, r: 12, reps: 8}, {c: 45, k: 21, r: 12, reps: 4}],
    [{c: 33, k: 11, r: 11, reps: 12}, {c: 34, k: 12, r: 11, reps: 4}],
  ],
  [                                       // Version 14
    [{c: 145, k: 115, r: 15, reps: 3}, {c: 146, k: 116, r: 15, reps: 1}],
    [{c: 64, k: 40, r: 12, reps: 4}, {c: 65, k: 41, r: 12, reps: 5}],
    [{c: 36, k: 16, r: 10, reps: 11}, {c: 37, k: 17, r: 10, reps: 5}],
    [{c: 36, k: 12, r: 12, reps: 11}, {c: 37, k: 13, r: 12, reps: 5}],
  ],
  [                                       // Version 15
    [{c: 109, k: 87, r: 11, reps: 5}, {c: 110, k: 88, r: 11, reps: 1}],
    [{c: 65, k: 41, r: 12, reps: 5}, {c: 66, k: 42, r: 12, reps: 5}],
    [{c: 54, k: 24, r: 15, reps: 5}, {c: 55, k: 25, r: 15, reps: 7}],
    [{c: 36, k: 12, r: 12, reps: 11}, {c: 37, k: 13, r: 12, reps: 7}],
  ],
  [                                       // Version 16
    [{c: 122, k: 98, r: 12, reps: 5}, {c: 123, k: 99, r: 12, reps: 1}],
    [{c: 73, k: 45, r: 14, reps: 7}, {c: 74, k: 46, r: 14, reps: 3}],
    [{c: 43, k: 19, r: 12, reps: 15}, {c: 44, k: 20, r: 12, reps: 2}],
    [{c: 45, k: 15, r: 15, reps: 3}, {c: 46, k: 16, r: 15, reps: 13}],
  ],
  [                                       // Version 17
    [{c: 135, k: 107, r: 14, reps: 1}, {c: 136, k: 108, r: 14, reps: 5}],
    [{c: 74, k: 46, r: 14, reps: 10}, {c: 75, k: 47, r: 14, reps: 1}],
    [{c: 50, k: 22, r: 14, reps: 1}, {c: 51, k: 23, r: 14, reps: 15}],
    [{c: 42, k: 14, r: 14, reps: 2}, {c: 43, k: 15, r: 14, reps: 17}],
  ],
  [                                       // Version 18
    [{c: 150, k: 120, r: 15, reps: 5}, {c: 151, k: 121, r: 15, reps: 1}],
    [{c: 69, k: 43, r: 13, reps: 9}, {c: 70, k: 44, r: 13, reps: 4}],
    [{c: 50, k: 22, r: 14, reps: 17}, {c: 51, k: 23, r: 14, reps: 1}],
    [{c: 42, k: 14, r: 14, reps: 2}, {c: 43, k: 15, r: 14, reps: 19}],
  ],
  [                                       // Version 19
    [{c: 141, k: 113, r: 14, reps: 3}, {c: 142, k: 114, r: 14, reps: 4}],
    [{c: 70, k: 44, r: 13, reps: 3}, {c: 71, k: 45, r: 13, reps: 11}],
    [{c: 47, k: 21, r: 13, reps: 17}, {c: 48, k: 22, r: 13, reps: 4}],
    [{c: 39, k: 13, r: 13, reps: 9}, {c: 40, k: 14, r: 13, reps: 16}],
  ],
  [                                       // Version 20
    [{c: 135, k: 107, r: 14, reps: 3}, {c: 136, k: 108, r: 14, reps: 5}],
    [{c: 67, k: 41, r: 13, reps: 3}, {c: 68, k: 42, r: 13, reps: 13}],
    [{c: 54, k: 24, r: 15, reps: 15}, {c: 55, k: 25, r: 15, reps: 5}],
    [{c: 43, k: 15, r: 14, reps: 15}, {c: 44, k: 16, r: 14, reps: 10}],
  ],
  [                                       // Version 21
    [{c: 144, k: 116, r: 14, reps: 4}, {c: 145, k: 117, r: 14, reps: 4}],
    [{c: 68, k: 42, r: 13, reps: 17}],
    [{c: 50, k: 22, r: 14, reps: 17}, {c: 51, k: 23, r: 14, reps: 6}],
    [{c: 46, k: 16, r: 15, reps: 19}, {c: 47, k: 17, r: 15, reps: 6}],
  ],
  [                                       // Version 22
    [{c: 139, k: 111, r: 14, reps: 2}, {c: 140, k: 112, r: 14, reps: 7}],
    [{c: 74, k: 46, r: 14, reps: 17}],
    [{c: 54, k: 24, r: 15, reps: 7}, {c: 55, k: 25, r: 15, reps: 16}],
    [{c: 37, k: 13, r: 12, reps: 34}],
  ],
  [                                       // Version 23
    [{c: 151, k: 121, r: 15, reps: 4}, {c: 152, k: 122, r: 15, reps: 5}],
    [{c: 75, k: 47, r: 14, reps: 4}, {c: 76, k: 48, r: 14, reps: 14}],
    [{c: 54, k: 24, r: 15, reps: 11}, {c: 55, k: 25, r: 15, reps: 14}],
    [{c: 45, k: 15, r: 15, reps: 16}, {c: 46, k: 16, r: 15, reps: 14}],
  ],
  [                                       // Version 24
    [{c: 147, k: 117, r: 15, reps: 6}, {c: 148, k: 118, r: 15, reps: 4}],
    [{c: 73, k: 45, r: 14, reps: 6}, {c: 74, k: 46, r: 14, reps: 14}],
    [{c: 54, k: 24, r: 15, reps: 11}, {c: 55, k: 25, r: 15, reps: 16}],
    [{c: 46, k: 16, r: 15, reps: 30}, {c: 47, k: 17, r: 15, reps: 2}],
  ],
  [                                       // Version 25
    [{c: 132, k: 106, r: 13, reps: 8}, {c: 133, k: 107, r: 13, reps: 4}],
    [{c: 75, k: 47, r: 14, reps: 8}, {c: 76, k: 48, r: 14, reps: 13}],
    [{c: 54, k: 24, r: 15, reps: 7}, {c: 55, k: 25, r: 15, reps: 22}],
    [{c: 45, k: 15, r: 15, reps: 22}, {c: 46, k: 16, r: 15, reps: 13}],
  ],
  [                                       // Version 26
    [{c: 142, k: 114, r: 14, reps: 10}, {c: 143, k: 115, r: 14, reps: 2}],
    [{c: 74, k: 46, r: 14, reps: 19}, {c: 75, k: 47, r: 14, reps: 4}],
    [{c: 50, k: 22, r: 14, reps: 28}, {c: 51, k: 23, r: 14, reps: 6}],
    [{c: 46, k: 16, r: 15, reps: 33}, {c: 47, k: 17, r: 15, reps: 4}],
  ],
  [                                       // Version 27
    [{c: 152, k: 122, r: 15, reps: 8}, {c: 153, k: 123, r: 15, reps: 4}],
    [{c: 73, k: 45, r: 14, reps: 22}, {c: 74, k: 46, r: 14, reps: 3}],
    [{c: 53, k: 23, r: 15, reps: 8}, {c: 54, k: 24, r: 15, reps: 26}],
    [{c: 45, k: 15, r: 15, reps: 12}, {c: 46, k: 16, r: 15, reps: 28}],
  ],
  [                                       // Version 28
    [{c: 147, k: 117, r: 15, reps: 3}, {c: 148, k: 118, r: 15, reps: 10}],
    [{c: 73, k: 45, r: 14, reps: 3}, {c: 74, k: 46, r: 14, reps: 23}],
    [{c: 54, k: 24, r: 15, reps: 4}, {c: 55, k: 25, r: 15, reps: 31}],
    [{c: 45, k: 15, r: 15, reps: 11}, {c: 46, k: 16, r: 15, reps: 31}],
  ],
  [                                       // Version 29
    [{c: 146, k: 116, r: 15, reps: 7}, {c: 147, k: 117, r: 15, reps: 7}],
    [{c: 73, k: 45, r: 14, reps: 21}, {c: 74, k: 46, r: 14, reps: 7}],
    [{c: 53, k: 23, r: 15, reps: 1}, {c: 54, k: 24, r: 15, reps: 37}],
    [{c: 45, k: 15, r: 15, reps: 19}, {c: 46, k: 16, r: 15, reps: 26}],
  ],
  [                                       // Version 30
    [{c: 145, k: 115, r: 15, reps: 5}, {c: 146, k: 116, r: 15, reps: 10}],
    [{c: 75, k: 47, r: 14, reps: 19}, {c: 76, k: 48, r: 14, reps: 10}],
    [{c: 54, k: 24, r: 15, reps: 15}, {c: 55, k: 25, r: 15, reps: 25}],
    [{c: 45, k: 15, r: 15, reps: 23}, {c: 46, k: 16, r: 15, reps: 25}],
  ],
  [                                       // Version 31
    [{c: 145, k: 115, r: 15, reps: 13}, {c: 146, k: 116, r: 15, reps: 3}],
    [{c: 74, k: 46, r: 14, reps: 2}, {c: 75, k: 47, r: 14, reps: 29}],
    [{c: 54, k: 24, r: 15, reps: 42}, {c: 55, k: 25, r: 15, reps: 1}],
    [{c: 45, k: 15, r: 15, reps: 23}, {c: 46, k: 16, r: 15, reps: 28}],
  ],
  [                                       // Version 32
    [{c: 145, k: 115, r: 15, reps: 17}],
    [{c: 74, k: 46, r: 14, reps: 10}, {c: 75, k: 47, r: 14, reps: 23}],
    [{c: 54, k: 24, r: 15, reps: 10}, {c: 55, k: 25, r: 15, reps: 35}],
    [{c: 45, k: 15, r: 15, reps: 19}, {c: 46, k: 16, r: 15, reps: 35}],
  ],
  [                                       // Version 33
    [{c: 145, k: 115, r: 15, reps: 17}, {c: 146, k: 116, r: 15, reps: 1}],
    [{c: 74, k: 46, r: 14, reps: 14}, {c: 75, k: 47, r: 14, reps: 21}],
    [{c: 54, k: 24, r: 15, reps: 29}, {c: 55, k: 25, r: 15, reps: 19}],
    [{c: 45, k: 15, r: 15, reps: 11}, {c: 46, k: 16, r: 15, reps: 46}],
  ],
  [                                       // Version 34
    [{c: 145, k: 115, r: 15, reps: 13}, {c: 146, k: 116, r: 15, reps: 6}],
    [{c: 74, k: 46, r: 14, reps: 14}, {c: 75, k: 47, r: 14, reps: 23}],
    [{c: 54, k: 24, r: 15, reps: 44}, {c: 55, k: 25, r: 15, reps: 7}],
    [{c: 46, k: 16, r: 15, reps: 59}, {c: 47, k: 17, r: 15, reps: 1}],
  ],
  [                                       // Version 35
    [{c: 151, k: 121, r: 15, reps: 12}, {c: 152, k: 122, r: 15, reps: 7}],
    [{c: 75, k: 47, r: 14, reps: 12}, {c: 76, k: 48, r: 14, reps: 26}],
    [{c: 54, k: 24, r: 15, reps: 39}, {c: 55, k: 25, r: 15, reps: 14}],
    [{c: 45, k: 15, r: 15, reps: 22}, {c: 46, k: 16, r: 15, reps: 41}],
  ],
  [                                       // Version 36
    [{c: 151, k: 121, r: 15, reps: 6}, {c: 152, k: 122, r: 15, reps: 14}],
    [{c: 75, k: 47, r: 14, reps: 6}, {c: 76, k: 48, r: 14, reps: 34}],
    [{c: 54, k: 24, r: 15, reps: 46}, {c: 55, k: 25, r: 15, reps: 10}],
    [{c: 45, k: 15, r: 15, reps: 2}, {c: 46, k: 16, r: 15, reps: 64}],
  ],
  [                                       // Version 37
    [{c: 152, k: 122, r: 15, reps: 17}, {c: 153, k: 123, r: 15, reps: 4}],
    [{c: 74, k: 46, r: 14, reps: 29}, {c: 75, k: 47, r: 14, reps: 14}],
    [{c: 54, k: 24, r: 15, reps: 49}, {c: 55, k: 25, r: 15, reps: 10}],
    [{c: 45, k: 15, r: 15, reps: 24}, {c: 46, k: 16, r: 15, reps: 46}],
  ],
  [                                       // Version 38
    [{c: 152, k: 122, r: 15, reps: 4}, {c: 153, k: 123, r: 15, reps: 18}],
    [{c: 74, k: 46, r: 14, reps: 13}, {c: 75, k: 47, r: 14, reps: 32}],
    [{c: 54, k: 24, r: 15, reps: 48}, {c: 55, k: 25, r: 15, reps: 14}],
    [{c: 45, k: 15, r: 15, reps: 42}, {c: 46, k: 16, r: 15, reps: 32}],
  ],
  [                                       // Version 39
    [{c: 147, k: 117, r: 15, reps: 20}, {c: 148, k: 118, r: 15, reps: 4}],
    [{c: 75, k: 47, r: 14, reps: 40}, {c: 76, k: 48, r: 14, reps: 7}],
    [{c: 54, k: 24, r: 15, reps: 43}, {c: 55, k: 25, r: 15, reps: 22}],
    [{c: 45, k: 15, r: 15, reps: 10}, {c: 46, k: 16, r: 15, reps: 67}],
  ],
  [                                       // Version 40
    [{c: 148, k: 118, r: 15, reps: 19}, {c: 149, k: 119, r: 15, reps: 6}],
    [{c: 75, k: 47, r: 14, reps: 18}, {c: 76, k: 48, r: 14, reps: 31}],
    [{c: 54, k: 24, r: 15, reps: 34}, {c: 55, k: 25, r: 15, reps: 34}],
    [{c: 45, k: 15, r: 15, reps: 20}, {c: 46, k: 16, r: 15, reps: 61}],
  ],
]


var numberOfBlocksPerVersion = new Array(41);
for (let i = 1; i < table13.length; i++) {
  numberOfBlocksPerVersion[i] = new Array(4);
  for (let j = 0; j < 4; j++) {
    numberOfBlocksPerVersion[i][j] = 0;
    for (let k = 0; k < table13[i][j].length; k++) {
      numberOfBlocksPerVersion[i][j] += table13[i][j][k].reps;
    }
  }
}

var dataCapacityPerVersion = new Array(41);
for (let i = 1; i < table13.length; i++) {
  dataCapacityPerVersion[i] = new Array(4);
  for (let j = 0; j < 4; j++) {
    dataCapacityPerVersion[i][j] = 0;
    for (let k = 0; k < table13[i][j].length; k++) {
      dataCapacityPerVersion[i][j] += table13[i][j][k].k * table13[i][j][k].reps;
    }
  }
}

// This function performs a sanity check
// It looks at tables 7 and 13 and compares values to make sure they're correct
// It's not fool proof but it did help me pinpoint an incorrect value in t7
function checkTable13() {
  for (let i = 1; i < table13.length; i++) {
    for (let j = 0; j < 4; j++) {
      if (table13[i][j].length == 2) {
	if (table13[i][j][0].c != table13[i][j][1].c - 1) {
	  console.log("something seems odd with 'c' in table 13 for version " + i + " level " + j);
	}
	let numberOfECCBytes = 0;
	let numberOfBytes = 0;
	for (let k = 0; k < table13[i][j].length; k++) {
	  numberOfECCBytes += (table13[i][j][k].c - table13[i][j][k].k) * table13[i][j][k].reps;
	  numberOfBytes += table13[i][j][k].c * table13[i][j][k].reps;
	}
	if (table7[i][j] + numberOfECCBytes != numberOfBytes) {
	  console.log("something seems odd with in table 7 or 13 for version " + i + " level " + j);
	}
      }
    }
  }
}

function intStringToNumericModeBytes(n) {
  let numericMode = [false, false, false, true];
  let characterCount = n.length;
  let characterCountIndicatorLength = 0;
  if (symbolVersion < 10) {
    characterCountIndicatorLength = 10;
  } else if (symbolVersion < 27) {
    characterCountIndicatorLength = 12;
  } else {
    characterCountIndicatorLength = 14;
  }
  characterCountIndicator = intToBytes(characterCount, characterCountIndicatorLength);
  let result = [];
  result = result.concat(characterCountIndicator);
  result = numericMode.concat(result);
  for (let i = 0; i < Math.floor(n.length / 3); i++) {
    let groupValue = 0;
    groupValue += parseInt(n[3 * i + 0]) * 100;
    groupValue += parseInt(n[3 * i + 1]) * 10;
    groupValue += parseInt(n[3 * i + 2]) * 1;
    result = result.concat(intToBytes(groupValue, 10));
  }

  let groupValue = 0;
  for (let i = 3 * Math.floor(n.length / 3); i < n.length; i++) {
    groupValue *= 10;
    groupValue += parseInt(n[i]);
  }
  if (Math.floor(n.length / 3) % 3 == 1) {
    result = result.concat(intToBytes(groupValue, 4));
  } else if (Math.floor(n.length / 3) % 3 == 2) {
    result = result.concat(intToBytes(groupValue, 7));
  }

  return result;
}

var table5 = [
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
  'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
  'U', 'V', 'W', 'X', 'Y', 'Z', ' ', '$', '%', '*',
  '+', '-', '.', '/', ':'
];


// TODO: complete this
var table6 = [
  '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
  '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
  '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
  '\0', '\0',
  ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')',
  '*', '+', ',', '-', '.', '/', '0', '1', '2', '3',
  '4', '5', '6', '7', '8', '9', ':', ';', '<', '=',
  '>', '?',
  '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
  'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
  'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']',
  '^', '_',
  '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
  'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
  't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}',
  '~', '\0',
  '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
  '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
  '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
  '\0', '\0',
  '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
  '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
  '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
  '\0', '\0',
  '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
  '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
  '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
  '\0', '\0',
];

var reverseTable5 = {
  '0': 0, 
  '1': 1,
  '2': 2,
  '3': 3,
  '4': 4,
  '5': 5,
  '6': 6,
  '7': 7,
  '8': 8,
  '9': 9,
  'A': 10,
  'B': 11,
  'C': 12,
  'D': 13,
  'E': 14,
  'F': 15,
  'G': 16,
  'H': 17,
  'I': 18,
  'J': 19,
  'K': 20,
  'L': 21,
  'M': 22,
  'N': 23,
  'O': 24,
  'P': 25,
  'Q': 26,
  'R': 27,
  'S': 28,
  'T': 29,
  'U': 30,
  'V': 31,
  'W': 32,
  'X': 33,
  'Y': 34,
  'Z': 35,
  ' ': 36,
  '$': 37,
  '%': 38,
  '*': 39,
  '+': 40,
  '-': 41,
  '.': 42,
  '/': 43,
  ':': 44,
  'a': 10,
  'b': 11,
  'c': 12,
  'd': 13,
  'e': 14,
  'f': 15,
  'g': 16,
  'h': 17,
  'i': 18,
  'j': 19,
  'k': 20,
  'l': 21,
  'm': 22,
  'n': 23,
  'o': 24,
  'p': 25,
  'q': 26,
  'r': 27,
  's': 28,
  't': 29,
  'u': 30,
  'v': 31,
  'w': 32,
  'x': 33,
  'y': 34,
  'z': 35,
};

// TODO: complete this
var reverseTable6 = {
  ' ': 32,
  '!': 33,
  '"': 34,
  '#': 35,
  '$': 36,
  '%': 37,
  '&': 38,
  '\'': 39,
  '(': 40,
  ')': 41,
  '*': 42,
  '+': 43,
  ',': 44,
  '-': 45,
  '.': 46,
  '/': 47,
  '0': 48,
  '1': 49,
  '2': 50,
  '3': 51,
  '4': 52,
  '5': 53,
  '6': 54,
  '7': 55,
  '8': 56,
  '9': 57,
  ':': 58,
  ';': 59,
  '<': 60,
  '=': 61,
  '>': 62,
  '?': 63,
  '@': 64,
  'A': 65,
  'B': 66,
  'C': 67,
  'D': 68,
  'E': 69,
  'F': 70,
  'G': 71,
  'H': 72,
  'I': 73,
  'J': 74,
  'K': 75,
  'L': 76,
  'M': 77,
  'N': 78,
  'O': 79,
  'P': 80,
  'Q': 81,
  'R': 82,
  'S': 83,
  'T': 84,
  'U': 85,
  'V': 86,
  'W': 87,
  'X': 88,
  'Y': 89,
  'Z': 90,
  '[': 91,
  '\\': 92,
  ']': 93,
  '^': 94,
  '_': 95,
  '`': 96,
  'a': 97,
  'b': 98,
  'c': 99,
  'd': 100,
  'e': 101,
  'f': 102,
  'g': 103,
  'h': 104,
  'i': 105,
  'j': 106,
  'k': 107,
  'l': 108,
  'm': 109,
  'n': 110,
  'o': 111,
  'p': 112,
  'q': 113,
  'r': 114,
  's': 115,
  't': 116,
  'u': 117,
  'v': 118,
  'w': 119,
  'x': 120,
  'y': 121,
  'z': 122,
  '{': 123,
  '|': 124,
  '}': 125,
  '~': 126,
  '\0': 127,
};

function stringToAlphanumericModeBytes(data) {
  let numericMode = [false, false, true, false];
  let characterCount = data.length;
  let characterCountIndicatorLength = 0;
  if (symbolVersion < 10) {
    characterCountIndicatorLength = 9;
  } else if (symbolVersion < 27) {
    characterCountIndicatorLength = 11;
  } else {
    characterCountIndicatorLength = 13;
  }
  characterCountIndicator = intToBytes(characterCount, characterCountIndicatorLength);
  let result = [];
  result = result.concat(characterCountIndicator);
  result = numericMode.concat(result);
  for (let i = 0; i < Math.floor(data.length / 2); i++) {
    let groupValue = 0;
    for (let j = 0; j < 11; j++) {
      charPositionInBits[2 * i + 0].push(result.length + j);
      charPositionInBits[2 * i + 1].push(result.length + j);
      bitPositionInString[result.length + j].push(2 * i + 0);
      bitPositionInString[result.length + j].push(2 * i + 1);
    }
    groupValue += reverseTable5[data[2 * i + 0]] * 45;
    groupValue += reverseTable5[data[2 * i + 1]];
    result = result.concat(intToBytes(groupValue, 11));
  }

  if (data.length % 2 == 1) {
    for (let j = 0; j < 6; j++) {
      charPositionInBits[data.length - 1].push(result.length + j);
      bitPositionInString[result.length + j].push(data.length - 1);
    }
    result = result.concat(intToBytes(reverseTable5[data[characterCount - 1]], 6));
  }

  return result;
}

function stringToByteModeBytes(data) {
  let numericMode = [false, true, false, false];
  let characterCount = data.length;
  let characterCountIndicatorLength = 0;
  if (symbolVersion < 10) {
    characterCountIndicatorLength = 8;
  } else {
    characterCountIndicatorLength = 16;
  }
  characterCountIndicator = intToBytes(characterCount, characterCountIndicatorLength);
  let result = [];
  result = result.concat(characterCountIndicator);
  result = numericMode.concat(result);
  for (let i = 0; i < data.length; i++) {
    let groupValue = 0;
    for (let j = 0; j < 8; j++) {
      charPositionInBits[i].push(result.length + j);
      bitPositionInString[result.length + j].push(i);
    }
    groupValue += reverseTable6[data[i]];
    result = result.concat(intToBytes(groupValue, 8));
  }

  return result;
}

function addTerminatorAndPaddingBitsAndBytes(dataBits) {
  let terminator = [false, false, false, false];
  result = dataBits;

  result = result.concat(terminator);

  while (result.length % 8 != 0) {
    result = result.concat([false]);
  }

  // adding padding bytes
  padCodeword1 = [true, true, true, false, true, true, false, false];
  padCodeword2 = [false, false, false, true, false, false, false, true];
  let i = 0;
  while (result.length / 8 < table7[symbolVersion][errorCorrectionLevel]) {
    if (i % 2 == 0) {
      result = result.concat(padCodeword1);
    } else {
      result = result.concat(padCodeword2);
    }
    i += 1;
  }

  return result;
}


function plotDataAndECCBits(bits, onlyLines) {
  let x = symbolSize - 1;
  let y = symbolSize - 1;
  let verticalDirection = "up";
  let horizontalPosition = "right"
  ctx.beginPath();
  ctx.lineWidth = 2;
  ctx.strokeStyle = "rgb(255,0,0)";
  ctx.moveTo(scaleUpFactor * x + scaleUpFactor / 2, scaleUpFactor * y + scaleUpFactor / 2);
  for (let i = 0; i < bits.length; i++) {
    if (!busyModules[x][y]) {
      ctx.lineTo(scaleUpFactor * x + scaleUpFactor / 2, scaleUpFactor * y + scaleUpFactor / 2);
      if (!onlyLines) {
	pixelPositionInString[x][y] = bitFinalPositionInString[i];
	for (let j = 0; j < bitFinalPositionInString[i].length; j++) {
	  charCoordinatesOnSymbol[bitFinalPositionInString[i][j]].push({x:x,y:y});
	}
	setModule(x, y, bits[i], false)
      }
    } else {
      i--;
    }
    if (verticalDirection == "up") {
      if (horizontalPosition == "right") {
	horizontalPosition = "left";
	x--;
	if (x == 6) {
	  x--;
	}
      } else if (horizontalPosition == "left") {
	horizontalPosition = "right"
	if (y == 0) {
	  x--;
	  if (x == 6) {
	    x--;
	  }
	  verticalDirection = "down";
	} else {
	  x = x + 1;
	  y = y - 1;
	}
      }
    } else {
      if (horizontalPosition == "right") {
	horizontalPosition = "left";
	x = x - 1;
      } else if (horizontalPosition == "left") {
	horizontalPosition = "right"
	if (y == symbolSize - 1) {
	  x = x - 1;
	  verticalDirection = "up";
	} else {
	  x = x + 1;
	  y = y + 1;
	}
      }
    }
  }
}

function bitsToBlocks(bits) {
  let numberOfBlocks = 0;
  let blocksInfo = table13[symbolVersion][errorCorrectionLevel];
  let blockSizes = new Array();
  for (let i = 0; i < blocksInfo.length; i++) {
    numberOfBlocks += blocksInfo[i].reps;
    for (let j = 0; j < blocksInfo[i].reps; j++) {
      blockSizes = blockSizes.concat([blocksInfo[i].k]);
    }
  }

  blockBitPositionInString = new Array(numberOfBlocks);
  let result = new Array(numberOfBlocks);
  let bitCounter = 0;

  for (let i = 0; i < numberOfBlocks; i++) {
    result[i] = new Array(blockSizes[i] * 8);
    blockBitPositionInString[i] = new Array(blockSizes[i] * 8);
    for (let j = 0; j < blockSizes[i] * 8; j++) {
      blockBitPositionInString[i][j] = []
      result[i][j] = bits[bitCounter];
      for (let k = 0; k < bitPositionInString[bitCounter].length; k++) {
	let bo = {block: i, offset: j};
	charPositionInBitBlocks[bitPositionInString[bitCounter][k]].push(bo);
	blockBitPositionInString[i][j].push(bitPositionInString[bitCounter][k])
      }
      bitCounter++;
    }
  }

  return result;
}


function bitsToInts(bits) {
  let result = new Array(bits.length / 8);

  for (let i = 0; i < bits.length / 8; i++) {
    let digiti = 0;
    for (let j = i*8; j < (i+1)*8; j++) {
      digiti = digiti * 2;
      if (bits[j]) {
	digiti++;
      }
    }
    result[i] = digiti;
  }

  return result;
}


function intsToBits(ints) {
  let result = new Array(ints.length * 8);

  for (let i = 0; i < ints.length; i++) {
    let intsi = ints[i];
    for (let j = 7; j >=  0; j--) {
      if (intsi % 2 == 0) {
	result[i*8 + j] = false;
      } else {
	result[i*8 + j] = true;
      }
      intsi = Math.floor(intsi / 2);
    }
  }

  return result;
}


function getECCBlock(bits) {
  let block = bitsToInts(bits);
  let table13Entry = table13[symbolVersion][errorCorrectionLevel];
  let ecInfo = {};
  for (let i = 0; i < table13Entry.length; i++) {
    if (table13Entry[i].k == block.length) {
      ecInfo = table13Entry[i];
      break;
    }
  }
  let numErrorWords = ecInfo.c - ecInfo.k;
  let gen = tableA1[numErrorWords];
  let errorBlock = new Array(block.length);
  for (let i = 0; i < block.length; i++) {
    errorBlock[i] = block[i];
  }

  for (let i = 0; i < numErrorWords; i++) {
    errorBlock.push(0);
  }

  for (let k = 0; k < block.length; k++) {
    let coef = errorBlock[k];
    if (coef != 0) {
      let lcoef = galoisLog[coef];
      for (let n = 0; n < numErrorWords; n++) {
	errorBlock[k + n + 1] ^= galoisExp[lcoef + gen[n]];
      }
    }
  }

  let result = intsToBits(errorBlock);

  return result.slice(block.length * 8);
}

function interleaveAndConcat(dataBlocks, ECCBlocks) {
  let result = [];
  let numDataBlocks = dataBlocks.length;
  let numOfDataBytes = 0;
  for (let i = 0; i < numDataBlocks; i++) {
    numOfDataBytes += dataBlocks[i].length / 8;
  }
  let offsets = new Array(numDataBlocks);
  for (let i = 0; i < numDataBlocks; i++) {
    offsets[i] = 0;
  }
  for (let i = 0; i < numOfDataBytes; i++) {
    for (let j = 0; j < numDataBlocks; j++) {
      let idx = (i+j) % numDataBlocks;
      if (offsets[idx] < dataBlocks[idx].length) {
	for (let k = 0; k < 8; k++) {
	  for (let l = 0; l < blockBitPositionInString[idx][offsets[idx]+k].length; l++) {
	    charPositionInFinalBits[blockBitPositionInString[idx][offsets[idx]+k][l]].push(result.length);
	  }
	  bitFinalPositionInString[result.length] = new Set(blockBitPositionInString[idx][offsets[idx]+k]);
	  result.push(dataBlocks[idx][offsets[idx] + k]);
	}
	offsets[idx] += 8;
	break;
      }
    }
  }
  let numECCBlocks = ECCBlocks.length;
  let numOfECCBytes = 0;
  for (let i = 0; i < numECCBlocks; i++) {
    numOfECCBytes += ECCBlocks[i].length / 8;
  }
  let ECCoffsets = new Array(numECCBlocks);
  for (let i = 0; i < numECCBlocks; i++) {
    ECCoffsets[i] = 0;
  }
  for (let i = 0; i < numOfECCBytes; i++) {
    for (let j = 0; j < numECCBlocks; j++) {
      let idx = (i+j) % numECCBlocks;
      if (ECCoffsets[idx] < ECCBlocks[idx].length) {
	for (let k = 0; k < 8; k++) {
	  for (let l = 0; l < dataBlocks[idx].length; l++) {
	    //bitFinalPositionInString[result.length] = [...new Set(bitFinalPositionInString[result.length].concat(blockBitPositionInString[idx][l]))];
	    for (let m = 0; m < blockBitPositionInString[idx][l].length; m++) {
	      bitFinalPositionInString[result.length].add(blockBitPositionInString[idx][l][m]);
	    }
	    //console.log(bitFinalPositionInString);
	    //bitFinalPositionInString[result.length] = bitFinalPositionInString[result.length].concat(blockBitPositionInString[idx][ECCoffsets[l]);

	  }
	  result.push(ECCBlocks[idx][ECCoffsets[idx] + k]);
	}
	ECCoffsets[idx] += 8;
	break;
      }
    }
  }
  for (let i = 0; i < bitFinalPositionInString.length; i++) {
    bitFinalPositionInString[i] = [...bitFinalPositionInString[i]];
  }

  return result;
}

function drawDataAndECC() {
  //checkTable13();
  // Compute binary data
  //dataBits = intStringToNumericModeBytes("01234567");
  //dataBits = dataBits.concat(stringToAlphanumericModeBytes("AC-42"));
  //dataBits = stringToAlphanumericModeBytes("https://michaelpaper.xyz");
  //dataBits = stringToAlphanumericModeBytes(qrContent);
  dataBits = stringToByteModeBytes(qrContent);
  // Add terminator and padding bits and bytes
  dataBits = addTerminatorAndPaddingBitsAndBytes(dataBits);
  if (dataBits.length / 8 > dataCapacityPerVersion[symbolVersion][errorCorrectionLevel]) {
    return 1;
  }
  // Divide data into blocks using table13
  codewordBlocks = bitsToBlocks(dataBits);

  // Compute ECC for each piece of data
  ECCBlocks = new Array(codewordBlocks.length);
  for (let i = 0; i < codewordBlocks.length; i++) {
    ECCBlocks[i] = getECCBlock(codewordBlocks[i]);
  }

  // Interleave data codewords and ECC codewords
  dataAndECCBits = interleaveAndConcat(codewordBlocks, ECCBlocks);
  // Draw them
  plotDataAndECCBits(dataAndECCBits, false);

  return 0;
}


function applyMaskingPattern() {
  for (let i = 0; i < symbolSize; i++) {
    for (let j = 0; j < symbolSize; j++) {
      if (!busyModules[i][j]) {
	let should_switch = false;
	switch(maskingPattern) {
	  case 0:
	    should_switch = (i+j) % 2 == 0;
	    break;
	  case 1:
	    should_switch = j % 2 == 0;
	    break;
	  case 2:
	    should_switch = i % 3 == 0;
	    break;
	  case 3:
	    should_switch = (i+j) % 3 == 0;
	    break;
	  case 4:
	    should_switch = (Math.floor(j/2) + Math.floor(i/3)) % 2 == 0;
	    break;
	  case 5:
	    should_switch = ((i * j) % 2) + ((i * j) % 3) == 0;
	    break;
	  case 6:
	    should_switch = (((i * j) % 2) + ((i * j) % 3)) % 2 == 0;
	    break;
	  case 7:
	    should_switch = (((i + j) % 2) + ((i * j) % 3)) % 2 == 0;
	    break;
	  case 8: // /!\ CUSTOM MODE FOR VISUALIZATION, NON-STANDARD /!\
	    should_switch = false;
	    break;
	}
	if (should_switch) {
	  setModule(i, j, !symbolState[i][j], false);
	}
      }
    }
  }
  if (drawBitsOrder) {
    ctx.stroke();
  }
}


function draw() {
  // drawing the position detection patterns
  drawPositionDetectionPatterns();
  // drawing the timing patterns
  drawTimingPatterns();
  // drawing alignment patterns
  drawAlignmentPatterns();

  // drawing the format information
  drawFormatInformation();
  // drawing the version information
  drawVersionInformation();
  // drawing content
  if (drawDataAndECC() != 0) {
    canvas.style.display = "none";
    document.getElementById("error-paragraph").style.display = "block";
    return;
  }
  canvas.style.display = "inline-block";
  document.getElementById("error-paragraph").style.display = "none";
  // Apply mask
  applyMaskingPattern();
  if (drawGrid) {
    drawFrames();
  }
}

function  getMousePos(evt) {
  var rect = canvas.getBoundingClientRect(), // abs. size of element
    scaleX = canvas.width / rect.width,    // relationship bitmap vs. element for x
    scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for y

  return {
    x: (evt.clientX - rect.left) * scaleX,   // scale mouse coordinates after they have
    y: (evt.clientY - rect.top) * scaleY     // been adjusted to be relative to element
  }
}



function redrawQrCode() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  form = document.getElementById("qr-parameters-form").elements
  qrContent = form["qr-text"].value;
  drawBitsOrder = form["qr-bits-order"].checked;
  drawGrid = form["qr-grid"].checked;
  symbolVersion = Number(form["qr-version"].value);
  symbolSize = 17 + 4 * symbolVersion;
  for (let i = 0; i < symbolSize; i++) {
    symbolState[i] = new Array(symbolSize);
    for (let j = 0; j < symbolSize; j++) {
      symbolState[i][j] = false;
    }
  }
  busyModules = new Array(symbolSize);
  for (let i = 0; i < symbolSize; i++) {
    busyModules[i] = new Array(symbolSize);
    for (let j = 0; j < symbolSize; j++) {
      busyModules[i][j] = false;
    }
  }
  scaleUpFactor = Number(form["qr-scale"].value);
  displaySize = symbolSize * scaleUpFactor;
  errorCorrectionLevel = Number(form["qr-ecc"].value);
  maskingPattern = Number(form["qr-masking"].value);

  canvas.width = displaySize;
  canvas.height = displaySize;

  charPositionInBits = new Array(qrContent.length);
  for (let i = 0; i < charPositionInBits.length; i++) {
    charPositionInBits[i] = [];
  }
  bitPositionInString = new Array(symbolSize * symbolSize);
  for (let i = 0; i < bitPositionInString.length; i++) {
    bitPositionInString[i] = [];
  }
  charPositionInBitBlocks = new Array(qrContent.length);
  for (let i = 0; i < charPositionInBitBlocks.length; i++) {
    charPositionInBitBlocks[i] = [];
  }
  blockBitPositionInString = new Array(numberOfBlocksPerVersion[symbolVersion][errorCorrectionLevel]);;
  for (let i = 0; i < blockBitPositionInString.length; i++) {
    blockBitPositionInString[i] = [];
  }

  charPositionInFinalBits = new Array(qrContent.length);
  for (let i = 0; i < charPositionInFinalBits.length; i++) {
    charPositionInFinalBits[i] = [];
  }
  bitFinalPositionInString = new Array(symbolSize * symbolSize);
  for (let i = 0; i < bitFinalPositionInString.length; i++) {
    bitFinalPositionInString[i] = new Set();
  }

  charCoordinatesOnSymbol = new Array(qrContent.length);
  for (let i = 0; i < qrContent.length; i++) {
    charCoordinatesOnSymbol[i] = [];
  }
  pixelPositionInString = new Array(symbolSize);
  for (let i = 0; i < symbolSize; i++) {
    pixelPositionInString[i] = new Array(symbolSize);
    for (let j = 0; j < symbolSize; j++) {
      pixelPositionInString[i][j] = [];
    }
  }
  pixelPositionInPositionPattern = new Array(symbolSize);
  for (let i = 0; i < symbolSize; i++) {
    pixelPositionInPositionPattern[i] = new Array(symbolSize);
    for (let j = 0; j < symbolSize; j++) {
      pixelPositionInPositionPattern[i][j] = [];
    }
  }
  pixelPositionInTimingPatterns = new Array(symbolSize);
  for (let i = 0; i < symbolSize; i++) {
    pixelPositionInTimingPatterns[i] = new Array(symbolSize);
    for (let j = 0; j < symbolSize; j++) {
      pixelPositionInTimingPatterns[i][j] = false;
    }
  }
  pixelPositionInAlignmentPatterns = new Array(symbolSize);
  for (let i = 0; i < symbolSize; i++) {
    pixelPositionInAlignmentPatterns[i] = new Array(symbolSize);
    for (let j = 0; j < symbolSize; j++) {
      pixelPositionInAlignmentPatterns[i][j] = false;
    }
  }
  pixelPositionInMetadata = new Array(symbolSize);
  for (let i = 0; i < symbolSize; i++) {
    pixelPositionInMetadata[i] = new Array(symbolSize);
    for (let j = 0; j < symbolSize; j++) {
      pixelPositionInMetadata[i][j] = false;
    }
  }

  positionPatternCoordinatesOnSymbol["tr"] = [];
  positionPatternCoordinatesOnSymbol["tl"] = [];
  positionPatternCoordinatesOnSymbol["bl"] = [];

  timingPatternsCoordinatesOnSymbol = new Array((symbolSize - 16) * 2);
  alignmentPatternsCoordinatesOnSymbol = [];
  metadataCoordinatesOnSymbol = [];

  draw();

  let preview = document.getElementById("qr-code-text-preview");
  preview.innerHTML = "";
  for (let i = 0; i < qrContent.length; i++) {
    let span = document.createElement("span");
    let spanLetter = document.createTextNode(qrContent[i]);
    span.appendChild(spanLetter);
    span.classList.add("qr-code-text-preview-letter");
    span.setAttribute("onmouseenter", "letterMouseEnter(" + i + ")");
    span.setAttribute("onmouseleave", "letterMouseLeave(" + i + ")");
    preview.appendChild(span);
  }
  for (let i = 0; i < document.getElementsByClassName("position-pattern").length; i++) {
    document.getElementsByClassName("position-pattern")[i].setAttribute("onmouseenter", "positionPatternMouseEnter(" + i + ")");
    document.getElementsByClassName("position-pattern")[i].setAttribute("onmouseleave", "positionPatternMouseLeave(" + i + ")");
  }
  document.getElementsByClassName("timing-patterns")[0].setAttribute("onmouseenter", "timingPatternsMouseEnter()");
  document.getElementsByClassName("timing-patterns")[0].setAttribute("onmouseleave", "timingPatternsMouseLeave()");
  document.getElementsByClassName("alignment-patterns")[0].setAttribute("onmouseenter", "alignmentPatternsMouseEnter()");
  document.getElementsByClassName("alignment-patterns")[0].setAttribute("onmouseleave", "alignmentPatternsMouseLeave()");
  document.getElementsByClassName("metadata")[0].setAttribute("onmouseenter", "metadataMouseEnter()");
  document.getElementsByClassName("metadata")[0].setAttribute("onmouseleave", "metadataMouseLeave()");
}

function letterMouseEnter(i) {
  let letters = document.getElementsByClassName("qr-code-text-preview-letter");
  redrawQrCode();
  letters[i].classList.add("qr-code-text-preview-letter-hovered");
  for (let j = 0; j < charCoordinatesOnSymbol[i].length; j++) {
    // green
    ctx.fillStyle = "rgba(0,255,0,0.7)";
    ctx.fillRect(charCoordinatesOnSymbol[i][j].x * scaleUpFactor, charCoordinatesOnSymbol[i][j].y * scaleUpFactor, scaleUpFactor, scaleUpFactor);
  }
}

function letterMouseLeave(i) {
  redrawQrCode();
  let letters = document.getElementsByClassName("qr-code-text-preview-letter");
  letters[i].classList.remove("qr-code-text-preview-letter-hovered");
}

function positionPatternMouseEnter(cornerId) {
  let corner = cornerId == 0 ? "bl" : cornerId == 1 ? "tl" : "tr";
  let ppCorner = document.getElementById("position-pattern-" + corner);
  redrawQrCode();
  ppCorner.classList.add("position-pattern-hovered");
  for (let j = 0; j < positionPatternCoordinatesOnSymbol[corner].length; j++) {
    ctx.fillStyle = "rgba(0,255,0,0.7)";
    ctx.fillRect(positionPatternCoordinatesOnSymbol[corner][j].x * scaleUpFactor, positionPatternCoordinatesOnSymbol[corner][j].y * scaleUpFactor, scaleUpFactor, scaleUpFactor);
  }
}

function positionPatternMouseLeave(cornerId) {
  let corner = cornerId == 0 ? "bl" : cornerId == 1 ? "tl" : "tr";
  let ppCorner = document.getElementById("position-pattern-" + corner);
  redrawQrCode();
  ppCorner.classList.remove("position-pattern-hovered");
}

function timingPatternsMouseEnter() {
  let pp = document.getElementById("timing-patterns");
  redrawQrCode();
  pp.classList.add("timing-patterns-hovered");
  for (let j = 0; j < timingPatternsCoordinatesOnSymbol.length; j++) {
    ctx.fillStyle = "rgba(0,255,0,0.7)";
    ctx.fillRect(timingPatternsCoordinatesOnSymbol[j].x * scaleUpFactor, timingPatternsCoordinatesOnSymbol[j].y * scaleUpFactor, scaleUpFactor, scaleUpFactor);
  }
}

function timingPatternsMouseLeave() {
  let pp = document.getElementById("timing-patterns");
  redrawQrCode();
  pp.classList.remove("timing-patterns-hovered");
}

function alignmentPatternsMouseEnter() {
  let pp = document.getElementById("alignment-patterns");
  redrawQrCode();
  pp.classList.add("alignment-patterns-hovered");
  for (let j = 0; j < alignmentPatternsCoordinatesOnSymbol.length; j++) {
    ctx.fillStyle = "rgba(0,255,0,0.7)";
    ctx.fillRect(alignmentPatternsCoordinatesOnSymbol[j].x * scaleUpFactor, alignmentPatternsCoordinatesOnSymbol[j].y * scaleUpFactor, scaleUpFactor, scaleUpFactor);
  }
}

function alignmentPatternsMouseLeave() {
  let pp = document.getElementById("alignment-patterns");
  redrawQrCode();
  pp.classList.remove("alignment-patterns-hovered");
}

function metadataMouseEnter() {
  let pp = document.getElementById("metadata");
  redrawQrCode();
  pp.classList.add("metadata-hovered");
  for (let j = 0; j < metadataCoordinatesOnSymbol.length; j++) {
    ctx.fillStyle = "rgba(0,255,0,0.7)";
    ctx.fillRect(metadataCoordinatesOnSymbol[j].x * scaleUpFactor, metadataCoordinatesOnSymbol[j].y * scaleUpFactor, scaleUpFactor, scaleUpFactor);
  }
}

function metadataMouseLeave() {
  let pp = document.getElementById("metadata");
  redrawQrCode();
  pp.classList.remove("metadata-hovered");
}

redrawQrCode();

function mouseOverCanvas(evt) {
  let mouseCoord = getMousePos(evt);
  let hoveredPixelX = Math.floor(mouseCoord.x / scaleUpFactor);
  let hoveredPixelY = Math.floor(mouseCoord.y / scaleUpFactor);

  for (let i = 0; i < symbolSize; i++) {
    for (let j = 0; j < symbolSize; j++) {
      if (symbolState[i][j]) {
	ctx.fillStyle = "rgb(0,0,0)";
      } else {
	ctx.fillStyle = "rgb(255,255,255)";
      }
      ctx.fillRect(i * scaleUpFactor, j * scaleUpFactor, scaleUpFactor, scaleUpFactor);
    }
  }

  if (previousMouseCoord.x != hoveredPixelX || previousMouseCoord.y != hoveredPixelY) {
    ctx.clearRect(0, symbolSize * scaleUpFactor, canvas.width, canvas.height);
    ctx.font = "36px Courier New";
    let letters = document.getElementsByClassName("qr-code-text-preview-letter");
    for (let i = 0; i < qrContent.length; i++) {
      ctx.fillStyle = "rgb(0,0,0)";
      letters[i].classList.remove("qr-code-text-preview-letter-hovered");
      if (hoveredPixelX >= 0 && hoveredPixelX < symbolSize &&
	hoveredPixelY >= 0 && hoveredPixelY < symbolSize) {
	for (let j = 0; j < pixelPositionInString[hoveredPixelX][hoveredPixelY].length; j++) {
	  if (pixelPositionInString[hoveredPixelX][hoveredPixelY][j] == i) {
	    // green
	    letters[i].classList.add("qr-code-text-preview-letter-hovered");
	  }
	}
      }
    }
    for (let j = 0; j < document.getElementsByClassName("position-pattern").length; j++) {
      document.getElementsByClassName("position-pattern")[j].classList.remove("position-pattern-hovered");
    }
    document.getElementById("timing-patterns").classList.remove("timing-patterns-hovered");
    document.getElementById("alignment-patterns").classList.remove("alignment-patterns-hovered");
    document.getElementById("metadata").classList.remove("metadata-hovered");
    if (hoveredPixelX > 0 && hoveredPixelY > 0) {
      for (let j = 0; j < pixelPositionInPositionPattern[hoveredPixelX][hoveredPixelY].length; j++) {
	if (pixelPositionInPositionPattern[hoveredPixelX][hoveredPixelY].length > 0) {
	  // green
	  document.getElementById("position-pattern-" + pixelPositionInPositionPattern[hoveredPixelX][hoveredPixelY][0]).classList.add("position-pattern-hovered");
	}
      }
      if (pixelPositionInTimingPatterns[hoveredPixelX][hoveredPixelY]) {
        document.getElementById("timing-patterns").classList.add("timing-patterns-hovered");
      }
      if (pixelPositionInAlignmentPatterns[hoveredPixelX][hoveredPixelY]) {
        document.getElementById("alignment-patterns").classList.add("alignment-patterns-hovered");
      }
      if (pixelPositionInMetadata[hoveredPixelX][hoveredPixelY]) {
        document.getElementById("metadata").classList.add("metadata-hovered");
      }
    }
  }
  if (hoveredPixelX >= 0 && hoveredPixelX < symbolSize &&
    hoveredPixelY >= 0 && hoveredPixelY < symbolSize) {
    previousMouseCoord.x = hoveredPixelX;
    previousMouseCoord.y = hoveredPixelY;
    // orange
    ctx.fillStyle = "rgba(0,255,0,0.7)";
    ctx.fillRect(hoveredPixelX * scaleUpFactor, hoveredPixelY * scaleUpFactor, scaleUpFactor, scaleUpFactor);
  }
  plotDataAndECCBits(dataAndECCBits, true);
  if (drawBitsOrder) {
    ctx.stroke();
  }
  if (drawGrid) {
    drawFrames();
  }

}



document.getElementById("qr-parameters-form").addEventListener("submit", (e) => {
  e.preventDefault();
  redrawQrCode();
});


function toggleDetailedDescription(id) {
  var moreText = document.getElementById(id);

  if (moreText.style.display != "block") {
    moreText.style.display = "block";
  } else {
    moreText.style.display = "none";
  }
}

function toggleBitsOrder() {
  toggleDetailedDescription("detailed-description-bits-order");
}
function toggleVersion() {
  toggleDetailedDescription("detailed-description-version");
}
function toggleECC() {
  toggleDetailedDescription("detailed-description-ecc");
}
function toggleMasking() {
  toggleDetailedDescription("detailed-description-masking");
}
